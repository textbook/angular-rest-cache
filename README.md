Angular REST Cache
==================

Modular, REST-aware caching interceptor for the Angular `HttpClient`.
