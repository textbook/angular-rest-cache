/*
 * Public API Surface of angular-rest-cache
 */

export * from './lib/angular-rest-cache.service';
export * from './lib/angular-rest-cache.component';
export * from './lib/angular-rest-cache.module';
