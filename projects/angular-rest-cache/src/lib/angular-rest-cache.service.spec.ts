import { TestBed, inject } from '@angular/core/testing';

import { AngularRestCacheService } from './angular-rest-cache.service';

describe('AngularRestCacheService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AngularRestCacheService]
    });
  });

  it('should be created', inject([AngularRestCacheService], (service: AngularRestCacheService) => {
    expect(service).toBeTruthy();
  }));
});
