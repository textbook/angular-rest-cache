import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngularRestCacheComponent } from './angular-rest-cache.component';

describe('AngularRestCacheComponent', () => {
  let component: AngularRestCacheComponent;
  let fixture: ComponentFixture<AngularRestCacheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AngularRestCacheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngularRestCacheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
