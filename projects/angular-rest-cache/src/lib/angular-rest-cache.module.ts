import { NgModule } from '@angular/core';
import { AngularRestCacheComponent } from './angular-rest-cache.component';

@NgModule({
  imports: [
  ],
  declarations: [AngularRestCacheComponent],
  exports: [AngularRestCacheComponent]
})
export class AngularRestCacheModule { }
